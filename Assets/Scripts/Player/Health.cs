﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using FMODUnity;

public class Health : MonoBehaviour
{
    public static int myHealth = 15;
    public int numOfHearts;

    [SerializeField] private StudioEventEmitter _soundEmitter;
    [SerializeField] private Animator _animator;

    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    private void Update()
    {
        if( myHealth <= -100)
        {
            return;
        }
        if (myHealth <= 0)
        {
            myHealth = -100;
            _animator.SetTrigger("Death");
        }

        CheckingHealths();

        DecreaseHealth();
        IncreaseHealth();


      
    }
    public void GameOver()
    {
        _soundEmitter.SendMessage("Stop");
        SceneManager.LoadScene(1);
    }

    public void TookDamageFromEnemy(int damage)
    {
        // play a hurt sound 
        
        myHealth -= damage;
        Debug.Log("I HAVE GOT DAMAGE !" + damage);
        //dazedTime = startDazedTime;
    }
    private void DecreaseHealth()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            myHealth--;
            Debug.Log(myHealth);
        }
    }

    private void IncreaseHealth()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            myHealth++;
            Debug.Log(myHealth);
        }
    }

    private void CheckingHealths()
    {
        if (myHealth > numOfHearts)
        {
            myHealth = numOfHearts;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < myHealth)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }

            if (i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }
}
