﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TutorialScript : MonoBehaviour
{

    [SerializeField] private PlayableDirector _director;
    private void OnTriggerEnter2D(Collider2D _collision)
    {
        _director.Play();
    }
}
