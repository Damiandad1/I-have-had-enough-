﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPlayer : MonoBehaviour
{
	public Rigidbody2D _playerPrefab;
	//   [SerializeField] private GameObject _currentEnemy;
	private float stoppingDistance = 2.5f;
	private float _speed = 5f;
	public int enemyHealth = 3;
	public GameObject hitEffect;
	[SerializeField] private GameObject _enemy;
	float faceLeft;
	Vector3 characterScale;
	float characterScaleX;

	private bool isFacingLeft;
	public float characterMovePos;
	private float nextAttackTime;

	public Vector2 posEnem;
	public Animator animator;
	private void Awake()
	{
		faceLeft = _enemy.transform.localScale.x;

		characterScale = transform.localScale;
		characterScaleX = characterScale.x;
	}

	private void Update()
	{
		if (posEnem.x > 0)
		{
			//_enemy.transform.localScale.x = -8;
			characterScale.x = characterScaleX;
		}
		else
		{
			characterScale.x -= characterScaleX;
		}

		characterMovePos = Input.GetAxis("Horizontal");

		//if (Vector2.Distance(transform.position, _playerPrefab.position) < stoppingDistance)
		//{
		//	animator.SetTrigger("Attack");
		//	transform.position = Vector2.MoveTowards(transform.position, new Vector2(_playerPrefab.position.x, transform.position.y), _speed * Time.deltaTime);


		if (Health.myHealth <= 0 )
		{
			animator.SetTrigger("Triumph");
		}
		//}

		if (Vector2.Distance(transform.position, _playerPrefab.position) > stoppingDistance)
		{
			animator.SetBool("IsWalking", true);

			transform.position = Vector2.MoveTowards(transform.position, new Vector2(_playerPrefab.position.x, transform.position.y), _speed * Time.deltaTime);
			


		}
		else
		{
			animator.SetBool("IsWalking", false);
		}
		if (enemyHealth <= 0)
		{
			StartCoroutine(WaitForDeath());
			//	EnemyBoss._enemyAmount--;
			

		}
		if (Vector2.Distance(transform.position, _playerPrefab.position) <= stoppingDistance && _playerPrefab.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
			animator.SetBool("IsAttacking", true);
			if (Time.time > nextAttackTime)
			{

				Health.myHealth--;
				float fireRate = 1f;
				nextAttackTime = Time.time + fireRate;
				//	animator.SetBool("IsAttacking", false);
				animator.SetBool("IsAttacking", false);
			}
		}

	}
	private IEnumerator WaitForDeath()
	{
		animator.SetTrigger("Dead");
		yield return new WaitForSeconds(2f);
		Destroy(this.gameObject);
	}
	public void TakeMeleeDamage(int damage)
	{
		// play a hurt sound 
		Instantiate(hitEffect, transform.position, Quaternion.identity);
		enemyHealth -= damage;
		Debug.Log("damage TAKEN !" + damage);


		//dazedTime = startDazedTime;
	}


}
